const profile = {
  'firstName': 'Eduardo',
  'lastName': 'Gonçalves',
  'limit': 850.00
}

const products = [
  { 'id': 1, 'title': 'Celular', 'price': 500.01, 'inventory': 2, 'shipping': 15.00, 'imagem':'/assets/logo.png'},
  { 'id': 2, 'title': 'Cabo HDMI', 'price': 10.99, 'inventory': 10, 'shipping': 5.00, 'imagem':'/assets/logo.png'},
  { 'id': 3, 'title': 'Memoria DDR3', 'price': 19.99, 'inventory': 3, 'shipping': 22.50, 'imagem':'/assets/logo.png'},
  { 'id': 4, 'title': 'Teclado', 'price': 30.99, 'inventory': 5, 'shipping': 9.00, 'imagem':'/assets/logo.png'},
  { 'id': 5, 'title': 'Placa de Video', 'price': 487.00, 'inventory': 1, 'shipping': 35.00, 'imagem':'/assets/logo.png'},
  { 'id': 6, 'title': 'Mouse', 'price': 59.59, 'inventory': 6, 'shipping': 11.00, 'imagem':'/assets/logo.png'},
  { 'id': 7, 'title': 'Fone de Ouvido', 'price': 29.00, 'inventory': 2, 'shipping': 18.00, 'imagem':'/assets/logo.png'},
  { 'id': 8, 'title': 'MousePad', 'price': 12.99, 'inventory': 4, 'shipping': 6.00, 'imagem':'/assets/logo.png'},
  { 'id': 9, 'title': 'Gabinete', 'price': 109.10, 'inventory': 10, 'shipping': 22.70, 'imagem':'/assets/logo.png'},
  { 'id': 10, 'title': 'PenDrive', 'price': 13.99, 'inventory': 3, 'shipping': 3.50, 'imagem':'/assets/logo.png'},
  { 'id': 11, 'title': 'WebCam', 'price': 30.99, 'inventory': 5, 'shipping': 7.90, 'imagem':'/assets/logo.png'},
  { 'id': 12, 'title': 'HeadSet', 'price': 75.00, 'inventory': 5, 'shipping': 25.00, 'imagem':'/assets/logo.png'}
]

const promotions = [
  { 'id': 1, 'title': '30% Desconto' },
  { 'id': 2, 'title': 'R$100.00 Desconto' },
  { 'id': 3, 'title': 'Frete Grátis' },
  { 'id': 4, 'title': 'Mais R$100.00 de limite' }
]

export default {
  getProfile (cb) {
    setTimeout(() => cb(profile), 200)
  },

  getProducts (cb) {
    setTimeout(() => cb(products), 200)
  },

  getPromotions (cb) {
    setTimeout(() => cb(promotions), 200)
  }
}
